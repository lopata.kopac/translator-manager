import { QueryClient, QueryClientProvider } from 'react-query';
import './App.css';
import TranslateJobsTable from './features/TranslateJobsTable/translate-jobs-table';
import { Route, Routes } from 'react-router-dom';

const queryClient = new QueryClient()

function App() {

  return (
    <>
      <QueryClientProvider client={queryClient}>
      <Routes> 
        <Route path="/" element={<TranslateJobsTable />}></Route>
      </Routes>
      </QueryClientProvider>
    </>
  )
}

export default App
