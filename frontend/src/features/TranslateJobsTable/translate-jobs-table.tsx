
import React, { useEffect, useState } from 'react';
import { TranslateJob, TranslateJobPerPage } from '../../models/translate-job';
import { getTranslateJobsCall } from '../../calls/get-translate-jobs.call';
import { useQuery } from 'react-query';
import { Button, Form, Input, Table, TablePaginationConfig } from 'antd';
import { useParams, useNavigate } from 'react-router-dom';

interface TranslateJobsTableProps {

}

const TranslateJobsTable: React.FC<TranslateJobsTableProps> = () => {
    const [page, setPage] = useState(1);
    const [filter, setFilter] = useState<Partial<TranslateJob>>({});
    const { filter: filterParam, page: pageParam} = useParams();
    const navigate = useNavigate();

    const { isLoading, error, data } = useQuery(['TranslateJobs', filter, page], () => getTranslateJobsCall(filter, page, TranslateJobPerPage));
    
    const jobs = (data?.data.items || []).map((job: TranslateJob) => ({...job, key: job.id,}));
    const total = data?.data.totalCount || 0;

    const handleTableChange = (pagination: TablePaginationConfig) => {
        setPage(pagination.current || 1);
    };

    const onFilterFormFinish = (values: Partial<TranslateJob>) => {
        setFilter(values);
        navigate(`/?filter=${JSON.stringify(values)}&page=${page}`);
    }

    const clearFilterHandle = () => {
        setFilter({});
        navigate(`/`);
    }

    useEffect(() => {
        if (filterParam) {
            setFilter(JSON.parse(filterParam));
        }
        if (pageParam) {
            setPage(parseInt(pageParam));
        }
    }, [filterParam, pageParam]);

    if (error) {
        return <div>Something went wrong</div>;
    }

    return (
        <div>
            <Form
                layout="inline"
                onFinish={onFilterFormFinish}
            >
                <Form.Item
                    name="customerName"
                >
                <Input placeholder="Customer name" /> 
                </Form.Item>
                <Button type="primary" htmlType="submit">
                        Filter
                </Button>
                <Button type="primary" onClick={clearFilterHandle}>
                        Clear Filter
                </Button>
            </Form>
            <Table 
                dataSource={jobs}
                columns={columns}
                loading={isLoading}
                onChange={handleTableChange}
                pagination={{
                    total,
                    current: page,
                    pageSize: TranslateJobPerPage,
                    showSizeChanger: false,
                }}
            />
        </div>
    );
};

const columns = [
    {
        title: 'Id',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'CustomerName',
        dataIndex: 'customerName',
        key: 'customerName',
    },
    {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
    },
    {
        title: 'OriginalContent',
        dataIndex: 'originalContent',
        key: 'originalContent',
    },
    {
        title: 'TranslatedContent',
        dataIndex: 'translatedContent',
        key: 'translatedContent',
    },
    {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
    },
];

export default TranslateJobsTable;
