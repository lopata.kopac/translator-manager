export interface TranslateJob {
    id: number;
    customerName: string;
    status: string;
    originalContent: string;
    translatedContent: string;
    price: number;
}

export const TranslateJobPerPage = 5;