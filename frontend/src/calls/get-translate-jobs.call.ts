import axios from "axios"
import { TranslateJob } from "../models/translate-job"

export const getTranslateJobsCall = async (filter: Partial<TranslateJob>, page = 1, pageSize = 10) => {
  return await axios.get('http://localhost:5000/api/jobs/GetJobs', {
    params: {
        id: filter.id,
        customer_name: filter.customerName,
        status: filter.status,
        translate_dontent: filter.translatedContent,
        original_content: filter.originalContent,
        price: filter.price,
        page,
        page_size: pageSize
    }
  })
}