﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Xml;
using System.Xml.Linq;
using External.ThirdParty.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TranslationManagement.Api.Controlers;
using TranslationManagement.Api.Models;
using TranslationManagement.Api.Services;
using TranslationManagement.Domain;
using TranslationManagement.Infrastructure;

namespace TranslationManagement.Api.Controllers
{
    [ApiController]
    [Route("api/jobs/[action]")]
    public class TranslationJobController : ControllerBase
    {
        static class JobStatuses
        {
            internal static readonly string New = "New";
            internal static readonly string Inprogress = "InProgress";
            internal static readonly string Completed = "Completed";
        }

        private readonly ILogger<TranslatorManagementController> logger;
        private readonly ITranslationJobControllerService translationJobControllerService;

        public TranslationJobController(ITranslationJobControllerService translationJobControllerService, IServiceScopeFactory scopeFactory, ILogger<TranslatorManagementController> logger)
        {
            this.logger = logger;
            this.translationJobControllerService = translationJobControllerService;
        }

        [HttpGet]
        public PaginatedResult<TranslationJob> GetJobs(
            int? id = null,
            string customer_name = null,
            string status = null,
            string translate_dontent = null,
            string original_content = null,
            double? price = null,
            int page = 1,
            int page_size = 10
            )
        {
            return translationJobControllerService.GetMultiple(new Models.TranslationJobFilter { 
                Id =  id,
                CustomerName = customer_name,
                Status = status,
                TranslatedContent = translate_dontent,
                OriginalContent = original_content,
                Price = price,
            }, page, page_size);
        }

        [HttpGet]
        [Route("{id}")]
        public TranslationJob GetJob(int id)
        {
            return translationJobControllerService.GetFirst(id);
        }

        [HttpPost]
        public TranslationJob CreateJob(TranslationJob job)
        {
            var newJob = translationJobControllerService.Create(job);
            if (newJob != null)
            {
                var notificationSvc = new UnreliableNotificationService();
                while (!notificationSvc.SendNotification("Job created: " + newJob.Id).Result)
                {
                }

                logger.LogInformation("New job notification sent");
            }

            return newJob;
        }

        [HttpPost]
        public TranslationJob CreateJobWithFile(IFormFile file, string customer)
        {
            var reader = new StreamReader(file.OpenReadStream());
            string content;

            if (file.FileName.EndsWith(".txt"))
            {
                content = reader.ReadToEnd();
            }
            else if (file.FileName.EndsWith(".xml"))
            {
                var xdoc = XDocument.Parse(reader.ReadToEnd());
                content = xdoc.Root.Element("Content").Value;
                customer = xdoc.Root.Element("Customer").Value.Trim();
            }
            else
            {
                throw new NotSupportedException("unsupported file");
            }

            var job = translationJobControllerService.Create(new TranslationJob()
            {
                OriginalContent = content,
                TranslatedContent = "",
                CustomerName = customer,
            });

            translationJobControllerService.Create(job);

            return job;
        }

        [HttpPost]
        public string UpdateJobStatus(int jobId, int translatorId, string newStatus = "")
        {
            logger.LogInformation("Job status update request received: " + newStatus + " for job " + jobId.ToString() + " by translator " + translatorId);
            if (typeof(JobStatuses).GetProperties().Count(prop => prop.Name == newStatus) == 0)
            {
                return "invalid status";
            }

            var job = translationJobControllerService.GetFirst(jobId);

            if ((
                job.Status == JobStatuses.New && newStatus == JobStatuses.Completed) ||
                job.Status == JobStatuses.Completed ||
                newStatus == JobStatuses.New
                )
            {
                return "invalid status change";
            }

            translationJobControllerService.UpdateStatus(jobId, newStatus);

            return "updated";
        }
    }
}