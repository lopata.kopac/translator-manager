﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TranslationManagement.Api.Models;
using TranslationManagement.Api.Services;
using TranslationManagement.Domain;

namespace TranslationManagement.Api.Controlers
{
    [ApiController]
    [Route("api/TranslatorsManagement/[action]")]
    public class TranslatorManagementController : ControllerBase
    {
        public static readonly string[] TranslatorStatuses = { "Applicant", "Certified", "Deleted" };

        private readonly ILogger<TranslatorManagementController> logger;
        private ITranslatorManagementService translatorManagementService;

        public TranslatorManagementController(ITranslatorManagementService translatorManagement, IServiceScopeFactory scopeFactory, ILogger<TranslatorManagementController> logger)
        {
            this.logger = logger;
            this.translatorManagementService = translatorManagement;
        }

        [HttpGet]
        public PaginatedResult<TranslatorManagement> GetTranslators(
            int? id = null,
            string name = null,
            string hourly_rate = null,
            string status = null,
            string creditCard_number = null,
            int page = 1,
            int page_size = 10
            )
        {
            return translatorManagementService.GetMultiple(new TranslatorManagementFilter { 
                Id =  id,
                Name = name,
                HourlyRate = hourly_rate,
                Status = status,
                CreditCardNumber = creditCard_number,
            }, page, page_size);
        }

        [HttpGet]
        [Route("{id}")]
        public TranslatorManagement GetTranslatorsByName(int id)
        {
            return translatorManagementService.GetFirst(id);
        }

        [HttpPost]
        public TranslatorManagement AddTranslator(TranslatorManagement translator)
        {
            
            return translatorManagementService.Create(translator);
        }
        
        [HttpPatch]
        [Route("{id}")]
        public string UpdateTranslatorStatus(int id, [Required][FromBody]string newStatus = "")
        {
            logger.LogInformation("User status update request: " + newStatus + " for user " + id.ToString());
            if (TranslatorStatuses.Where(status => status == newStatus).Count() == 0)
            {
                throw new ArgumentException("unknown status");
            }

            translatorManagementService.UpdateStatus(id, newStatus);

            return "updated";
        }
    }
}