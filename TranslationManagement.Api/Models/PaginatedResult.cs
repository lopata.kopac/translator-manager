﻿namespace TranslationManagement.Api.Models
{
    public class PaginatedResult<T>
    {
        public T[] Items { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
