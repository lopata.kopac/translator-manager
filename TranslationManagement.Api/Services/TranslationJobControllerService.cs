﻿using Microsoft.AspNetCore.Http.Json;
using Microsoft.VisualBasic;
using System.Linq;
using TranslationManagement.Api.Models;
using TranslationManagement.Domain;
using TranslationManagement.Infrastructure;

namespace TranslationManagement.Api.Services
{
    public class TranslationJobControllerService : ITranslationJobControllerService
    {
        private AppDbContext context;

        const double PricePerCharacter = 0.01;

        public TranslationJobControllerService(AppDbContext context)
        {
            this.context = context;
        }

        public TranslationJob GetFirst(int id)
        {
            return context.TranslationJobs.FirstOrDefault(t => t.Id == id);
        }

        public TranslationJob GetFirst(TranslationJobFilter filter = null)
        {
            var query = context.TranslationJobs.AsQueryable();

            if (filter != null)
            {
                query = FilterImputToQuery(filter, query);
            }

            return query.FirstOrDefault();
        }

        public PaginatedResult<TranslationJob> GetMultiple(TranslationJobFilter filter = null, int page = 1, int pageSize = 10)
        {
            var query = context.TranslationJobs.AsQueryable();

            if (filter != null)
            {
                query = FilterImputToQuery(filter, query);
            }

            if (pageSize > 0)
            {
                query.Skip((page - 1) * pageSize).Take(pageSize).ToArray();
            }

            query.OrderBy(x => x.CustomerName);

            var results = query.ToArray();

            return new PaginatedResult<TranslationJob>
            {
                Items = results,
                TotalCount = results.Count(),
                Page = pageSize > 0 ? pageSize : results.Count(),
                PageSize = pageSize > 0 ? pageSize : 0
            };
        }
        
        public TranslationJob Create(TranslationJob job)
        {
            job.Status = "New";
            job.Price = job.OriginalContent.Length * PricePerCharacter;

            context.TranslationJobs.Add(job);

            if(context.SaveChanges() <= 0)
            {
                return null;
            }

            return job;
        }

        public int GetCount(TranslationJobFilter filter = null)
        {
            var query = context.TranslationJobs.AsQueryable();
            if(filter != null)
            {
                query = FilterImputToQuery(filter, query);
            }

            return query.Count();
        }

        public TranslationJob UpdateStatus(int id, string status)
        {
            var job = context.TranslationJobs.FirstOrDefault(t => t.Id == id);
            if(job != null)
            {
                return null;
            }

            job.Status = status;
            context.SaveChanges();

            return job;
        }

        private IQueryable<TranslationJob> FilterImputToQuery(TranslationJobFilter filter, IQueryable<TranslationJob> query)
        {
            if (filter.Id != null)
            {
                query = query.Where(t => t.Id == filter.Id);
            }

            if(!string.IsNullOrEmpty(filter.CustomerName))
            {
                query = query.Where(t => t.CustomerName == filter.CustomerName);
            }

            if (!string.IsNullOrEmpty(filter.Status))
            {
                query = query.Where(t => t.Status == filter.Status);
            }

            if (!string.IsNullOrEmpty(filter.OriginalContent))
            {
                query = query.Where(t => t.OriginalContent == filter.OriginalContent);
            }

            if (!string.IsNullOrEmpty(filter.TranslatedContent))
            {
                query = query.Where(t => t.TranslatedContent == filter.TranslatedContent);
            }

            if(filter?.Price != null)
            {
                query = query.Where(t => t.Price == filter.Price);
            }

            return query;
        }
    }
}
