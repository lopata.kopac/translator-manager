﻿using System.Linq;
using TranslationManagement.Api.Models;
using TranslationManagement.Domain;
using TranslationManagement.Infrastructure;

namespace TranslationManagement.Api.Services
{
    public class TranslatorManagementService : ITranslatorManagementService
    {
        private AppDbContext context;
        public TranslatorManagementService(AppDbContext context)
        {
            this.context = context;
        }

        public TranslatorManagement Create(TranslatorManagement translator)
        {
            context.Translators.Add(translator);

            if (context.SaveChanges() <= 0)
            {
                return null;
            }

            return translator;
        }

        public TranslatorManagement GetFirst(int id)
        {
            return context.Translators.FirstOrDefault(t => t.Id == id);
        }

        public TranslatorManagement GetFirst(TranslatorManagementFilter filter = null)
        {
            var query = context.Translators.AsQueryable();

            if (filter != null)
            {
                query = FilterImputToQuery(filter, query);
            }

            return query.FirstOrDefault();
        }

        public PaginatedResult<TranslatorManagement> GetMultiple(TranslatorManagementFilter filter = null, int page = 1, int pageSize = 10)
        {
            var query = context.Translators.AsQueryable();

            if (filter != null)
            {
                query = FilterImputToQuery(filter, query);
            }

            if (pageSize > 0)
            {
                query.Skip((page - 1) * pageSize).Take(pageSize).ToArray();
            }

            query.OrderBy(x => x.Name);

            var results = query.ToArray();

            return new PaginatedResult<TranslatorManagement>
            {
                Items = results,
                TotalCount = results.Count(),
                Page = pageSize > 0 ? pageSize : results.Count(),
                PageSize = pageSize > 0 ? pageSize : 0
            };
        }

        public int GetCount(TranslatorManagementFilter filter = null)
        {
            return context.Translators.Count();
        }

        public TranslatorManagement UpdateStatus(int id, string status)
        {
            var translator = context.Translators.FirstOrDefault(t => t.Id == id);
            if (translator != null)
            {
                return null;
            }

            translator.Status = status;
            context.SaveChanges();

            return translator;
        }

        private IQueryable<TranslatorManagement> FilterImputToQuery(TranslatorManagementFilter filter, IQueryable<TranslatorManagement> query)
        {
            if (filter.Id.HasValue)
            {
                query = query.Where(t => t.Id == filter.Id);
            }

            if (!string.IsNullOrEmpty(filter.Name))
            {
                query = query.Where(t => t.Name == filter.Name);
            }

            if (!string.IsNullOrEmpty(filter.Status))
            {
                query = query.Where(t => t.Status == filter.Status);
            }

            if (!string.IsNullOrEmpty(filter.HourlyRate))
            {
                query = query.Where(t => t.HourlyRate == filter.HourlyRate);
            }

            if(!string.IsNullOrEmpty(filter.CreditCardNumber))
            {
                query = query.Where(t => t.CreditCardNumber == filter.CreditCardNumber);
            }

            return query;
        }
    }
}
