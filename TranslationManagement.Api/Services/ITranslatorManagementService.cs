﻿using TranslationManagement.Api.Models;
using TranslationManagement.Domain;

namespace TranslationManagement.Api.Services
{
    public interface ITranslatorManagementService
    {
        TranslatorManagement GetFirst(int id);
        TranslatorManagement GetFirst(TranslatorManagementFilter filter = null);
        PaginatedResult<TranslatorManagement> GetMultiple(TranslatorManagementFilter filter = null, int page = 1, int pageSize = 10);
        TranslatorManagement Create(TranslatorManagement translator);
        int GetCount(TranslatorManagementFilter filter = null);
        TranslatorManagement UpdateStatus(int id, string status);
    }
}
