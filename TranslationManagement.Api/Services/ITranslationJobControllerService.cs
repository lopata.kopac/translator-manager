﻿using TranslationManagement.Api.Models;
using TranslationManagement.Domain;

namespace TranslationManagement.Api.Services
{
    public interface ITranslationJobControllerService
    {
        TranslationJob GetFirst(int id);
        TranslationJob GetFirst(TranslationJobFilter filter = null);
        PaginatedResult<TranslationJob> GetMultiple(TranslationJobFilter filter = null, int page = 1, int pageSize = 10);
        TranslationJob Create(TranslationJob job);
        int GetCount(TranslationJobFilter filter = null);
        TranslationJob UpdateStatus(int id, string status);
    }
}
