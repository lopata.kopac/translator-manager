﻿using Microsoft.EntityFrameworkCore;
using TranslationManagement.Domain;

namespace TranslationManagement.Infrastructure
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<TranslationJob> TranslationJobs { get; set; }
        public DbSet<TranslatorManagement> Translators { get; set; }
    }
}